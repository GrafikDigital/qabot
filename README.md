# QA Bot
## Notifications for QA document changes - closing the feedback loop between Dev and Design

This is bad code - initially it was good code, but it became bad during a nightmare debugging session

One day it may be good again

Set up an Incoming Webhook in Slack, make sure the QA document follows the right structure, or adjust the dictionary values accordingly

In GAS script editor, set up a trigger to run myOnEdit() every time the spreadsheet is edited
