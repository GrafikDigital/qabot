_OUTGOING_WEBHOOK_TOKEN_ = 'tzkudxhWM4T4Ke0zqRvILIgO';
user_range = 'A2:A11';
slack_username_range = 'G2:G11';
people = {};
sheetsToWatch = ['Mobile','Tablet','Desktop'];
column_index = {
  'browser':'A',
  'qa_person':'B',
  'page_url':'C',
  'component_name':'D',
  'task_description':'E',
  'assigned_to': 'F',
  'priority': 'G',
  'status': 'H'
}

alphabet = 'abcdefghijklmnopqrstuvwxyz'.toUpperCase().split('');
function determineAlphabetIndex(letter){
  return alphabet.indexOf(letter)+1;
}

function setupRecipients(){
  users = SpreadsheetApp.getActive().getSheetByName('Index').getRange(user_range).getValues();
  slack_usernames = SpreadsheetApp.getActive().getSheetByName('Index').getRange(slack_username_range).getValues();
  for (var i = 0; i < users.length; i++) {
    person = users[i][0];
    if(!people[person]){
      // initialize new users
      people[users[i][0]] = {'tasks':[]};
    }
    people[person]['slack_username'] = slack_usernames[i][0];
  }
}

function clearLogs(){
 Logger.log('')
}


function myOnEdit(e){
  setupRecipients();
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var cell = SpreadsheetApp.getActiveRange();
  var cellNotation = cell.getA1Notation();
  var timestamp = new Date();
  var currentSheet = ss.getActiveSheet();
  var currentSheetName = currentSheet.getName();
  var row = e.range.getRow();
  var col = e.range.getColumn();
  var num_cols_to_return = Object.keys(column_index).length;
  if(sheetsToWatch.indexOf(currentSheetName)>=0){
    if(e.range.getColumn() == determineAlphabetIndex(column_index['status'])){
      row_item = currentSheet.getRange(row, 1, 1, num_cols_to_return).getValues()[0];
      composeSlackMessage('update', row_item, currentSheetName);
    }else if(e.range.getColumn() == determineAlphabetIndex(column_index['task_description'])){
      if(e.oldValue == undefined && e.value != undefined){
        row_item = currentSheet.getRange(row, 1, 1, num_cols_to_return).getValues()[0];
        composeSlackMessage('new', row_item, currentSheetName);
      }
    }
  }
}

function composeSlackMessage(kind, entire_row, currentSheetName){
  Logger.log('composeSlackMessage '+kind);
  z = {
    'browser': entire_row[determineAlphabetIndex(column_index['browser']) - 1].toString(),
    'qa_person':entire_row[determineAlphabetIndex(column_index['qa_person']) - 1].toString(),
    'page_url': entire_row[determineAlphabetIndex(column_index['page_url']) - 1].toString(),
    'component_name': entire_row[determineAlphabetIndex(column_index['component_name']) - 1].toString(),
    'task_description': entire_row[determineAlphabetIndex(column_index['task_description']) - 1].toString(),
    'assigned_to': entire_row[determineAlphabetIndex(column_index['assigned_to']) - 1].toString(),
    'priority': entire_row[determineAlphabetIndex(column_index['priority']) - 1].toString(),
    'status': entire_row[determineAlphabetIndex(column_index['status']) - 1].toString()
  }
  if(kind=='update'){
    var send_the_message = true;
    switch(z['status']){
      case 'Fixed':
        // QA needs to validate
        person_of_interest = people[z['qa_person']]['slack_username'];
        prompt = "\n Please mark _Validated_ or provide feedback";
        break;
      case 'Validated':
        // Ticket closed
        person_of_interest = people[z['assigned_to']]['slack_username'];
        prompt = '\n Congrats!';
        break;
      case 'Rejected':
        // Ticket rejected
        person_of_interest = people[z['assigned_to']]['slack_username'];
        prompt = '\n Go to '+currentSheetName + ' row '+row+' and click the Description column for comments';
        break;
      case 'In Progress':
        send_the_message = false;
        break;
      default:
        send_the_message = false;
        break;
    }
    if(send_the_message){
      var message = "hey \@" + person_of_interest +"! "+currentSheetName+ " - *" +z['component_name'] +"*: "+z['task_description'] +" is _" + z['status'] + "_" + prompt;
    }else{
      return;
    }
  }else if(kind=='new'){
    if(z['qa_person']){}else{z['qa_person'] = 'Someone';}
    Logger.log(z['qa_person']);
    var message = z['qa_person'] + " added a new QA item (_" + z['task_description'] + "_) to "+currentSheetName + "! ";
    Logger.log(message);
    if((z['assigned_to']).length < 1){
      message += "It's currently unassigned.";
    }else{
      message += "It's currently assigned to @"+people[z['assigned_to']]['slack_username'];
    }
  }
  Logger.log(message)
  var url = "https://hooks.slack.com/services/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";
  var payload = {
    "username" : "QABot",
    "text" : message,
    "link_names": 1,
    "icon_url" : "http://a1667.phobos.apple.com/us/r30/Purple3/v4/73/be/df/73bedfd0-15b0-0221-2893-7212abe9ed94/SheetsIcon-60_2x.png"
  }
  sendToSlack_(url, payload)
}


function sendToSlack_(url,payload) {
  Logger.log("sendToSlack_ called");
   var options =  {
    "method" : "post",
    "contentType" : "application/json",
    "payload" : JSON.stringify(payload)
  };
  try {
    return UrlFetchApp.fetch(url, options)
  } catch(e) {
    Logger.log(e)
  }
}
